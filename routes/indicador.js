const express = require('express');
const router = express.Router();
const indicador = require('../services/indicador');

/* GET indicador */
router.get('/', async function(req, res, next) {
  try {
    res.json(await indicador.getMultiple(req.query.page));
  } catch (err) {
    console.error(`Error while getting indicadores `, err.message);
    next(err);
  }
});

/* POST indicador */
router.post('/', async function(req, res, next) {
  try {
    res.json(await indicador.create(req.body));
  } catch (err) {
    console.error(`Error while creating indicador`, err.message);
    next(err);
  }
});

/* PUT indicador */
router.put('/:id', async function(req, res, next) {
  try {
    res.json(await indicador.update(req.params.id, req.body));
  } catch (err) {
    console.error(`Error while updating indicadores`, err.message);
    next(err);
  }
});

/* DELETE indicador */
router.delete('/:id', async function(req, res, next) {
  try {
    res.json(await indicador.remove(req.params.id));
  } catch (err) {
    console.error(`Error while deleting indicadores`, err.message);
    next(err);
  }
});

module.exports = router;