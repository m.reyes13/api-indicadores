const db = require('./db');
const helper = require('../helper');
const config = require('../config');

async function getMultiple(page = 1){
  const offset = helper.getOffset(page, config.listPerPage);
  const rows = await db.query(
    `SELECT id, nombre, categoria, responsable, definicion, objetivo, meta 
    FROM indicadores LIMIT ${offset},${config.listPerPage}`
  );
  const data = helper.emptyOrRows(rows);
  const meta = {page};

  return {
    data,
    meta
  }
}

async function create(indicador){
  const result = await db.query(
    `INSERT INTO indicadores 
    (nombre, categoria, responsable, definicion, objetivo, meta) 
    VALUES 
    ('${indicador.nombre}', '${indicador.categoria}', '${indicador.responsable}', '${indicador.definicion}', '${indicador.objetivo}', '${indicador.meta}')`
  );

  let message = 'Error in creating indicadores';

  if (result.affectedRows) {
    message = 'Indicadores created successfully';
  }

  return {message};
}

async function update(id, indicador){
  const result = await db.query(
    `UPDATE indicadores
    SET nombre="${indicador.nombre}", categoria=${indicador.categoria}, responsable=${indicador.responsable}, 
    definicion=${indicador.definicion}, objetivo=${indicador.objetivo} , meta=${indicador.meta}
    WHERE id=${id}` 
  );

  let message = 'Error in updating indicadores';

  if (result.affectedRows) {
    message = 'Indicadores updated successfully';
  }

  return {message};
}

async function remove(id){
  const result = await db.query(
    `DELETE FROM indicadores WHERE id=${id}`
  );

  let message = 'Error in deleting indicadores';

  if (result.affectedRows) {
    message = 'indicadores deleted successfully';
  }

  return {message};
}

module.exports = {
  getMultiple,
  create,
  update,
  remove
}